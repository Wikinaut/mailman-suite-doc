=============================
Architecture of Mailman Suite
=============================

Mailman Suite is a set of two functional components, Mailman Core and Web
Frontend. Web frontend is based on Django Web Framework and is often just
referred to as Django. The actual projects that use the Django web framework
are called Postorius (Web Frontend) and Hyperkitty (Archiver). From a runtime
perspective, Postorius and Hyperkitty can run in a single component.


.. image:: _static/mailman-architecture.png
   :width: 800px
   :alt: Mailman Suite Architecture


* **MTA**: Mail-Transfer-Agent is typically responsible for receiving emails
  from internet and sending out emails. It is configured to forward emails to
  Mailman owned mailing lists to Mailman Core's LMTP server via LMTP protocol.

  MTA also receives incoming requests to send out emails from both Mailman Core
  and Web Interface. Web interface will send out emails related to account
  management like verifying email addresses for example.

  Hyperkitty also allows replying to posts from the web interface, which are
  sent out as emails through the MTA, which then in-turn are forwarded to
  Mailman Core like any other incoming email.

* **Web Server**: Web server is typically just a frontend to Web Interface due
  to security and performance. It is configured as a reverse-proxy for the Web
  Interface.

  Web Frontend will support any web server that can act as a reverse proxy.

* **Mailman Core**: Mailman Core is a complex application and the above diagram
  only represents some parts of it. It listens primarily on two ports:

  * **REST API**: This is where Mailman's administrative REST API listens. This
    is used by the frontend to retrieve and show information to the user.

  * **LMTP Server**: This is how Mailman receives incoming emails from MTA.

* **Web Interface**: This includes both Hyperkitty and Postorius.

  * **Hyperkitty**: This receives emails that are to be archived from
    mailman-hyperkitty plugin for Mailman Core. It also hosts the archives
    for the mailing lists.

  * **Postorius**: This presents an interface to view/manage mailing lists
    and subscriptions. It also has various templates for mailing lists, which
    users can create to override the default ones provided by Mailman. Mailman
    Core will reach out to Postorius to fetch the templates when sending out
    emails.

++++++++++++++++++++++++++++++++++++++
Understanding User and Member Accounts
++++++++++++++++++++++++++++++++++++++

There are two "layers" where a user or member might exist:

* Mailman Core -> mailman3_core database
* Mailman Web -> mailman3_web database

First consider a situation which primarily involves Mailman Core. It is
possible for a user to visit a Mailman website and see this message: "You can
also subscribe without creating an account. If you wish to do so, please use
the form below." If they subscribes this way, entries will only be created in
the mailman_core database. An email address is added in the address table, and
information about subscribed mailing lists to the members table.

The member won't have a password, nor will they need one. They aren't able to
change or update a password.  Their credentials are limited to an email
address and lists they have subscribed to.  They don't have an "account" as
far as Mailman Web is concerned. And yet members of mailman3_core are able to
send and receive messages, and participate in the lists.

When importing a list from mailman2 to mailman3, this is the level of
membership the imported users will have. They will exist in mailman3_core and
can use the mailing listing, but they cannot sign into the website.

Next, consider Mailman Web. If you visit a mailman3 instance there are links
in the upper right corner of the screen to Login or Sign Up. This is the
method to create a Django account, and be able to log into the web interface,
creating entries in the auth_user and account_emailaddress tables of the
mailman3_web database. After generating a web account, the user is able to
manage more details about their subscriptions in the web UI.  If they subscribe
or unsubscribe from a list, that information is propagated down to the level of
mailman3_core.
